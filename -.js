const input = TestUtils.renderIntoDocument(<input onChange={sinon.spy()} />);
Promise.resolve().then(function(){
        TestUtils.Simulate.change(React.findDOMNode(input));
        return true;
}).then(function(result) {
    expect(result).to.be.true;
    expect(input.props.onChange).has.been.calledOnce;
}).then(done, done);
